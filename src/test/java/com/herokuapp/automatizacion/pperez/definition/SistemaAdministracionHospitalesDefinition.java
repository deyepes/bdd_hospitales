package com.herokuapp.automatizacion.pperez.definition;

import com.herokuapp.automatizacion.pperez.steps.SistemaAdminsitracionHospitalesSteps;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class SistemaAdministracionHospitalesDefinition {
	
	@Steps
	SistemaAdminsitracionHospitalesSteps sistemaAdminsitracionHospitalesSteps;
	
	@Given("^ingreso a la pagina web$")
	public void ingreso_a_la_pagina_web() throws Throwable {
		sistemaAdminsitracionHospitalesSteps.ingresarALaWeb();
	}
	
	@Given("^doy click en agregar Doctor$")
	public void doy_click_en_agregar_Doctor() throws Throwable {
		sistemaAdminsitracionHospitalesSteps.clickAgregarDoctor();
	}
	@Given("^doy click en agregar Paciente$")
	public void doy_click_en_agregar_Paciente() throws Throwable {
		sistemaAdminsitracionHospitalesSteps.clickAgregarPaciente();
	}
	
	@Given("^doy click en agendar una cita$")
	public void doy_click_en_agendar_una_cita() throws Throwable {
		sistemaAdminsitracionHospitalesSteps.clickAgendarCita();
	}
}
