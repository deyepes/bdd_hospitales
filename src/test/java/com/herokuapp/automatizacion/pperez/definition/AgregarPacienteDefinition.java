package com.herokuapp.automatizacion.pperez.definition;


import java.util.List;

import com.herokuapp.automatizacion.pperez.steps.AgregarPacienteSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AgregarPacienteDefinition {
	
	@Steps
	AgregarPacienteSteps agregarPacienteSteps;
	
	@When("^ingresa los datos del Paciente$")
	public void ingresa_los_datos_del_Paciente(DataTable datadb) throws Throwable {
		List<List<String>> data = datadb.raw();
		agregarPacienteSteps.ingresarDatosPaciente(data);
	}
}
