package com.herokuapp.automatizacion.pperez.definition;

import com.herokuapp.automatizacion.pperez.steps.AgendarCitaSteps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AgendarCitaDefinition {
	
	@Steps
	AgendarCitaSteps agendarCitaSteps;
	
	@When("^Se solicita una cita para el (\\d+)/(\\d+)/(\\d+)$")
	public void se_solicita_una_cita_para_el(String mes, String dia, String anio) throws Throwable {
		agendarCitaSteps.ingresarFecha(mes, dia, anio);
	}

	@When("^para el paciente (\\d+)$")
	public void para_el_paciente(String idPaciente) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		agendarCitaSteps.ingresarPaciente(idPaciente);
	}

	@When("^con el doctor (\\d+)$")
	public void con_el_doctor(String idDoctor) throws Throwable {
		agendarCitaSteps.ingresarDoctor(idDoctor);
	}
	
	@When("^con una Observacion \"([^\"]*)\"$")
	public void con_una_Observacion(String observacion) throws Throwable {
		agendarCitaSteps.ingresarObservacion(observacion);
	}
	
	@When("^dar click en Guardar$")
	public void dar_click_en_Guardar() throws Throwable {
		agendarCitaSteps.clickEnGuardar();
	}
	
}
