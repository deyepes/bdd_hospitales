package com.herokuapp.automatizacion.pperez.definition;

import com.herokuapp.automatizacion.pperez.steps.GeneralSteps;

import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class GeneralDefinition {
	
	@Steps
	GeneralSteps generalSteps;
	
	@Then("^verificar \"([^\"]*)\"$")
	public void verificar(String respuesta) throws Throwable {
		generalSteps.verificarRespuesta(respuesta);
	}
}
