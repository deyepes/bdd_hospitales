package com.herokuapp.automatizacion.pperez.definition;


import java.util.List;

import com.herokuapp.automatizacion.pperez.steps.AgregarDoctorSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AgregarDoctorDefinition {
	
	@Steps
	AgregarDoctorSteps agregarDoctorSteps;
	
	@When("^ingresa los datos del Doctor$")
	public void ingresa_los_datos_del_Doctor(DataTable datadb) throws Throwable {
		List<List<String>> data = datadb.raw();
		agregarDoctorSteps.ingresarDatosDoctor(data);
	}
}
