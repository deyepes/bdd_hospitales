package com.herokuapp.automatizacion.pperez;

import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.runner.RunWith;
import com.herokuapp.automatizacion.pperez.utilities.BeforeSuite;
import com.herokuapp.automatizacion.pperez.utilities.DataToFeature;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

@CucumberOptions (features = "src/test/resources/features/Citas.feature",tags= {"@AgendarUnaCita"}, snippets= SnippetType.CAMELCASE)
@RunWith(RunnerPersonalizado.class)
public class RunnerTags {
	@BeforeSuite
	public static void test() throws InvalidFormatException, IOException {
			DataToFeature.overrideFeatureFiles("./src/test/resources/features/Citas.feature");
	}
}
