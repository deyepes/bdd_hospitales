package com.herokuapp.automatizacion.pperez.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import net.serenitybdd.core.annotations.findby.FindBy;

public class GeneralPage extends PageObject {

	@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[2]/div[1]/h3")
	public WebElementFacade lblRespuesta;

	public String getRespuesta() {
		return lblRespuesta.getTextValue();
	}

}
