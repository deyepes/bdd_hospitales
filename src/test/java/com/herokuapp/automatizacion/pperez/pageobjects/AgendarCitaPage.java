package com.herokuapp.automatizacion.pperez.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;

public class AgendarCitaPage extends PageObject {

	@FindBy(id = "datepicker")
	public WebElementFacade txtFechaCita;
	
	@FindBy(xpath = "//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input")
	public WebElementFacade txtIdPaciente;
	
	@FindBy(xpath = "//*[@id=\"page-wrapper\"]/div/div[3]/div/div[3]/input")
	public WebElementFacade txtIdDoctor;
	
	@FindBy(xpath ="//*[@id=\"page-wrapper\"]/div/div[3]/div/a")
	public WebElement btnGuardar;
	
	@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/textarea")
	public WebElementFacade txtObservacion;
	
	@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[2]/div[1]/h3")
	public WebElementFacade lblRespuesta;

	
	public void ingresarFecha(String mes, String dia, String anio) {
		txtFechaCita.sendKeys(mes+"/"+dia+"/"+anio);
	}

	public void ingresarPaciente(String idPaciente) {
		txtIdPaciente.sendKeys(idPaciente);
	}

	public void ingresarDoctor(String idDoctor) {
		txtIdDoctor.sendKeys(idDoctor);
	}
	
	public void ingresarObservacion(String observacion) {
		txtObservacion.sendKeys(observacion);
	}

	public void clickEnGuardar() {
		btnGuardar.click();
	}

	public String getRespuesta() {
		return lblRespuesta.getTextValue();
	}

}
