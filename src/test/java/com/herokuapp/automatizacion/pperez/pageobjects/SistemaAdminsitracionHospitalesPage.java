package com.herokuapp.automatizacion.pperez.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class SistemaAdminsitracionHospitalesPage extends PageObject {

	@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[1]")
	public WebElement lnkAgregrarDoctor;
	
	@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[2]")
	public WebElement lnkAgregrarPaciente;
	
	@FindBy(xpath = "//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[6]")
	public WebElement lnkAgendarCita;

	public void clickAgendarCita() {
		lnkAgendarCita.click();
	}

	public void clickAgregarDoctor() {
		lnkAgregrarDoctor.click();
	}

	public void clickAgregarPaciente() {
		lnkAgregrarPaciente.click();
	}
}
