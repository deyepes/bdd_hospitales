package com.herokuapp.automatizacion.pperez.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;

public class AgregarPacientePage extends PageObject {

	@FindBy(name = "name")
	public WebElementFacade txtNombre;
	
	@FindBy(name = "last_name")
	public WebElementFacade txtApellido;
	
	@FindBy(name ="telephone")
	public WebElement txtTelefono;
	
	@FindBy(name="identification_type")
	public WebElementFacade lstTipoIdentificacion;
	
	@FindBy(name="identification")
	public WebElementFacade txtNumIdentificacion;
	
	@FindBy(name="prepaid")
	public WebElementFacade chkPrepagada;
	
	public void setNombre(String nombre) {
		txtNombre.sendKeys(nombre);
	}

	public void setApellido(String apellido) {
		txtApellido.sendKeys(apellido);
	}

	public void setTelefono(String telefono) {
		txtTelefono.sendKeys(telefono);
	}

	public void setTipoDoc(String tipoDocumento) {
		lstTipoIdentificacion.click();
		lstTipoIdentificacion.selectByVisibleText(tipoDocumento);
	}

	public void setNumeroDocumento(String numeroDocumento) {
		txtNumIdentificacion.sendKeys(numeroDocumento);
	}

	public void setTienePrepagada(String tienePrepagada) {
		boolean prepagada = Boolean.parseBoolean(tienePrepagada);
		if (prepagada) {
			chkPrepagada.click();
		}
	}


}
