package com.herokuapp.automatizacion.pperez.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;

public class AgregarDoctorPage extends PageObject {

	@FindBy(id = "name")
	public WebElementFacade txtNombre;
	
	@FindBy(id = "last_name")
	public WebElementFacade txtApellido;
	
	@FindBy(id ="telephone")
	public WebElement txtTelefono;
	
	@FindBy(id="identification_type")
	public WebElementFacade lstTipoIdentificacion;
	
	@FindBy(id="identification")
	public WebElementFacade txtNumIdentificacion;

	public void setNombre(String nombre) {
		txtNombre.sendKeys(nombre);
	}

	public void setApellido(String apellido) {
		txtApellido.sendKeys(apellido);
	}

	public void setTelefono(String telefono) {
		txtTelefono.sendKeys(telefono);
	}

	public void setTipoDoc(String tipoDocumento) {
		lstTipoIdentificacion.click();
		lstTipoIdentificacion.selectByVisibleText(tipoDocumento);
	}

	public void setNumeroDocumento(String numeroDocumento) {
		txtNumIdentificacion.sendKeys(numeroDocumento);
	}


}
