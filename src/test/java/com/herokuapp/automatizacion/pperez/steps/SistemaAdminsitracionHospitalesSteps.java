package com.herokuapp.automatizacion.pperez.steps;

import com.herokuapp.automatizacion.pperez.pageobjects.SistemaAdminsitracionHospitalesPage;

public class SistemaAdminsitracionHospitalesSteps {

	SistemaAdminsitracionHospitalesPage sistemaAdminsitracionHospitalesPage;
	
	public void ingresarALaWeb() {
		sistemaAdminsitracionHospitalesPage.open();
	}

	public void clickAgendarCita() {
		sistemaAdminsitracionHospitalesPage.clickAgendarCita();
	}

	public void clickAgregarDoctor() {
		sistemaAdminsitracionHospitalesPage.clickAgregarDoctor();
	}

	public void clickAgregarPaciente() {
		sistemaAdminsitracionHospitalesPage.clickAgregarPaciente();
	}

}
