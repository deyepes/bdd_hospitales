package com.herokuapp.automatizacion.pperez.steps;

import static org.junit.Assert.assertTrue;
import com.herokuapp.automatizacion.pperez.pageobjects.GeneralPage;
public class GeneralSteps {
	
	GeneralPage generalPage;

	public void verificarRespuesta(String respuesta) {
		assertTrue(respuesta.equals(generalPage.getRespuesta()));
	}

}
