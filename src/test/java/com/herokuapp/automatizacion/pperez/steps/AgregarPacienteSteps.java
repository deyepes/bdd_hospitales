package com.herokuapp.automatizacion.pperez.steps;

import java.util.List;

import com.herokuapp.automatizacion.pperez.pageobjects.AgregarPacientePage;

public class AgregarPacienteSteps {
	
	AgregarPacientePage agregarPacientePage;

	public void ingresarDatosPaciente(List<List<String>> data) {
		String nombre = data.get(0).get(0);
		String apellido = data.get(0).get(1);
		String telefono = data.get(0).get(2);
		String tipoDocumento = data.get(0).get(3);
		String numeroDocumento = data.get(0).get(4);
		String tienePrepagada =  data.get(0).get(5);
		
		agregarPacientePage.setNombre(nombre);
		agregarPacientePage.setApellido(apellido);
		agregarPacientePage.setTelefono(telefono);
		agregarPacientePage.setTipoDoc(tipoDocumento);
		agregarPacientePage.setNumeroDocumento(numeroDocumento);
		agregarPacientePage.setTienePrepagada(tienePrepagada);
	}

}
