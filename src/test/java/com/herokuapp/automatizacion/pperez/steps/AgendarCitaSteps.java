package com.herokuapp.automatizacion.pperez.steps;

import static org.junit.Assert.assertTrue;
import com.herokuapp.automatizacion.pperez.pageobjects.AgendarCitaPage;
public class AgendarCitaSteps {
	
	AgendarCitaPage agendarCitaPage;

	
	public void ingresarFecha(String mes, String dia, String anio) {
		agendarCitaPage.ingresarFecha(mes, dia, anio);
	}

	public void ingresarPaciente(String idPaciente) {
		agendarCitaPage.ingresarPaciente(idPaciente);
	}

	public void ingresarDoctor(String idDoctor) {
		agendarCitaPage.ingresarDoctor(idDoctor);
	}
	
	public void ingresarObservacion(String observacion) {
		agendarCitaPage.ingresarObservacion(observacion);
	}

	public void clickEnGuardar() {
		agendarCitaPage.clickEnGuardar();
	}

	public void verificarRespuesta(String respuesta) {
		assertTrue(respuesta.equals(agendarCitaPage.getRespuesta()));
	}

}
