package com.herokuapp.automatizacion.pperez.steps;

import java.util.List;

import com.herokuapp.automatizacion.pperez.pageobjects.AgregarDoctorPage;

public class AgregarDoctorSteps {
	
	AgregarDoctorPage agregarDoctorPage;

	public void ingresarDatosDoctor(List<List<String>> data) {
		String nombre = data.get(0).get(0);
		String apellido = data.get(0).get(1);
		String telefono = data.get(0).get(2);
		String tipoDocumento = data.get(0).get(3);
		String numeroDocumento = data.get(0).get(4);
		
		agregarDoctorPage.setNombre(nombre);
		agregarDoctorPage.setApellido(apellido);
		agregarDoctorPage.setTelefono(telefono);
		agregarDoctorPage.setTipoDoc(tipoDocumento);
		agregarDoctorPage.setNumeroDocumento(numeroDocumento);
	}

}
