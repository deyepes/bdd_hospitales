@Regresion
Feature: Sistema de administración de hospitales


  @AgregarUnDoctor
  Scenario Outline: Agregar doctor
    Given ingreso a la pagina web 
    And doy click en agregar Doctor
    When ingresa los datos del Doctor
   | <NombreCompleto>	 | <Apellidos>	 | <Telefono> 	| <TipoDoc>								|<DocIdentidad>		|
    And dar click en Guardar
    Then verificar "<resultado>"

    Examples: 
      | NombreCompleto	 | Apellidos	 | Telefono 	| TipoDoc								|DocIdentidad		|resultado			|
      | Doctor1					 | Apellido1	 | 1234567		| Cédula de ciudadanía	|100							|Guardado:			|

  @AgregarUnPaciente
  Scenario Outline: Agregar paciente
    Given ingreso a la pagina web 
    And doy click en agregar Paciente
    When ingresa los datos del Paciente
   | <NombreCompleto>	 | <Apellidos>	 | <Telefono> 	| <TipoDoc>								|<DocIdentidad>		| <tieneSaludPrepagada>	|
    And dar click en Guardar
    Then verificar "<resultado>"

    Examples: 
      | NombreCompleto	 | Apellidos	 | Telefono 	| TipoDoc								|DocIdentidad		|tieneSaludPrepagada		|resultado			|
      | Doctor1					 | Apellido1	 | 1234567		| Cédula de ciudadanía	|200							| true									|Guardado:			|


  @AgendarUnaCita
  Scenario Outline: Agendar una cita
    Given ingreso a la pagina web 
    And doy click en agendar una cita
    When Se solicita una cita para el <DiaDeLaCita> 
    And para el paciente <iDPaciente> 
    And con el doctor <iDDoctor> 	
    And con una Observacion "<observacion>"
    And dar click en Guardar
    Then verificar "<resultado>"

    Examples: 
   |id   | DiaDeLaCita  | iDPaciente	 | iDDoctor  | observacion		|resultado	|
   ##@externaldata@./src/test/resources/Datadriven/Datos.xlsx@AgendarCita@1
   |1   |06/19/2018   |201   |101   |observaciones   |Error:|
      
      
       @AgendarUnaCitaAlterno
  Scenario Outline: Agendar una cita sin doctor
    Given ingreso a la pagina web 
    And doy click en agendar una cita
    When Se solicita una cita para el <DiaDeLaCita> 
    And para el paciente <iDPaciente> 
    And con una Observacion "<observacion>"
    And dar click en Guardar
    Then verificar "<resultado>"

    Examples: 
      | DiaDeLaCita  | iDPaciente	 | iDDoctor 	 | observacion		|resultado	|
      | 06/19/2018	 |    		 203 | 101 				 | observaciones	|Error:			|
      
